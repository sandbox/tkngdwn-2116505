<?php

/**
 * @file
 * Administration forms for the Open AdStream jQuery module.
 */

/**
 * Menu callback; Displays the administration settings for Open AdStream jQuery.
 */
function openadstream_jquery_admin_settings() {
  $form = array();
  $form['openadstream_jquery_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Domain Path'),
    '#description' => t('The domain path you use to call Open AdStream. Do not include the "adstream_mjx.ads" portion of the url or anything after it. Do not include a trailing slash.'),
    '#default_value' => variable_get('openadstream_jquery_domain', ''),
  );
  $form['openadstream_jquery_positions'] = array(
    '#type' => 'textfield',
    '#title' => t('Positions'),
    '#description' => t('A comma separated list of available positions. (i.e. "x01,x02,x03")'),
    '#default_value' => variable_get('openadstream_jquery_positions', ''),
  );
  // Targeting settings.
  $form['targeting'] = array(
    '#type' => 'fieldset',
    '#title' => t('Targeting Options'),
  );
  $form['targeting']['openadstream_jquery_target_path'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use path targeting?'),
    '#description' => t('This will enable/disable using the path targeting in the query variable. (i.e. "path=node/123")'),
    '#default_value' => variable_get('openadstream_jquery_target_path', ''),
  );
  $form['targeting']['openadstream_jquery_target_type'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use type targeting?'),
    '#description' => t('This will enable/disable using the type targeting in the query variable. (i.e. "type=article")'),
    '#default_value' => variable_get('openadstream_jquery_target_type', ''),
  );
  $form['targeting']['openadstream_jquery_target_taxonomy'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use taxonomy targeting?'),
    '#description' => t('This will enable/disable using the taxonomy targeting in the query variable. (i.e. "taxonomy=category1,category2,category3")'),
    '#default_value' => variable_get('openadstream_jquery_target_taxonomy', ''),
  );
  $form['targeting']['openadstream_jquery_target_referer'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use referer targeting?'),
    '#description' => t('This will enable/disable passing the urlencoded referer in the query variable. This option may be unreliable as it uses the HTTP_REFERER property of the $_SERVER global. (i.e. "referer=http%3A%2F%2Fwww.somesite.com%2F")'),
    '#default_value' => variable_get('openadstream_jquery_target_referer', ''),
  );
  $form['targeting']['openadstream_jquery_target_site'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Target'),
    '#description' => t('This is passed as site=value. You may use this to target to a specific site. Leave this blank to ignore it.'),
    '#default_value' => variable_get('openadstream_jquery_target_site', ''),
  );
  // Interstitial settings.
  $form['interstitial'] = array(
    '#type' => 'fieldset',
    '#title' => t('Interstitial Options'),
  );
  $form['interstitial']['openadstream_jquery_interstitial_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable the interstitial?'),
    '#description' => t('This will enable/disable the interstitial ad which will display in a colorbox modal. This option requires that the colorbox module be enabled and installed.'),
    '#default_value' => variable_get('openadstream_jquery_interstitial_enabled', ''),
  );
  $form['interstitial']['openadstream_jquery_interstitial_position'] = array(
    '#type' => 'textfield',
    '#title' => t('Interstitial Postition'),
    '#description' => t('The position to use for the interstitial ad. (i.e. "x01")'),
    '#default_value' => variable_get('openadstream_jquery_interstitial_position', ''),
  );
  $form['interstitial']['openadstream_jquery_interstitial_frequency'] = array(
    '#type' => 'textfield',
    '#title' => t('Interstitial Frequency'),
    '#description' => t('The frequency to display the interstitial. (i.e. Every 4 Hours: "4h", Every 45 Minutes: "45m")'),
    '#default_value' => variable_get('openadstream_jquery_interstitial_frequency', '4h'),
  );
  $form['interstitial']['openadstream_jquery_interstitial_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Interstitial Title'),
    '#description' => t('The text to use for the colorbox title. (Use !duration for string replacement of the duration.)'),
    '#default_value' => variable_get('openadstream_jquery_interstitial_title', 'This advertisement will automatically close in !duration.'),
  );
  $form['interstitial']['openadstream_jquery_interstitial_duration'] = array(
    '#type' => 'textfield',
    '#title' => t('Interstitial Duration'),
    '#description' => t('The duration (in seconds) before automatically closing the interstitial.'),
    '#default_value' => variable_get('openadstream_jquery_interstitial_duration', 15),
  );
  $form['interstitial']['openadstream_jquery_interstitial_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Interstitial Size'),
    '#description' => t('The size of the modal for the interstitial in pixels wide x pixels high. (i.e. "600x400")'),
    '#default_value' => variable_get('openadstream_jquery_interstitial_size', '600x400'),
  );
  $form['interstitial']['openadstream_jquery_interstitial_disable'] = array(
    '#type' => 'textarea',
    '#title' => t('Disable on Pages'),
    '#description' => t('A list of pages to disable the interstitial on. One per line. Use * for wildcards. (i.e. "node/*"'),
    '#default_value' => variable_get('openadstream_jquery_interstitial_disable', ''),
  );
  return system_settings_form($form);
}
